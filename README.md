1. Dokończ wyrazy wg wzoru w lini nr 12.
2. Stwórz swój branch
3. Zakomituj na nim zmiany
4. Zmerguj develop do swojego brancha (git merge develop)
5. Rozwiąż konflikty -> https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/addressing-merge-conflicts/resolving-a-merge-conflict-using-the-command-line
6. Zrób commit z rozwiązanymi konfliktami
7. Wypushuj zmiany
8. Wyslij link swojego brancha z gitlaba na Slacku, żebym wiedział jak Ci poszło :) 

Podczas mergowania mastera do twojego brancha linie z dopiskiem "DO NOT CHANGE" pozostaw takie jak na masterze.

G-it

I-

T-


P-

U-

S-

H-
